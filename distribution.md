# holoProtocols (distribution)

### 7 steps protocols :

1. Write our introduction of holocomponents (README.md files)
2. Add workflowy content  to introduction if applicable 
3. Review presentations to add any additional info 
4. Create holoAtom (in a pad), and register url to holoLink
5. Create holocomponent’s assets
    * icon, slide, guide
    * bugs, issues (<https://framagit.org/holoteam/mlp/-/issues>)
    * enquiries (QAs) (<https://framagit.org/holoteam/enquiries/-/issues>)
    * interoperability (https://gitlab.com/holosphere4/holoBridges)
6. Distribute content to the following:
    1. white paper,
    2. git repository
    3. hyperlink guide to hologuide index in clickup docs https://app.clickup.com/2497563/v/dc/2c70v-596/2c70v-225 
    4. add as ML component on MLP clickup doc
7. Model it ... 
   
### links:

* git page: <https://holoTeam.frama.io/protocols> (git)
* git repository: <https://framagit.org/holoteam/protocols> (git)
* holoWhiteBoard: <https://mensuel.framapad.org/p/holoWhiteBoard> (pad)
* holoSphere OS: <https://docs.google.com/document/d/1e_feUAG1X4H9mtysAI8JRYw6dpQiSWnLSoaoPe1fyhs/> (WP)


