
### 7 steps protocols :

1. Write our introduction of holocomponents (README.md files)
2. Add [workflowy][wf] content  to introduction if applicable 
3. Review presentations to add any additional info 
4. Create [holoAtom][ha] (in a [pad][hap]), and register url to [holoLink][hl]
5. Create holocomponent’s assets
    a. icon, slide, guide
    2. bugs, issues (<https://framagit.org/holoteam/mlp/-/issues>)
    3. enquiries (QAs) (<https://framagit.org/holoteam/enquiries/-/issues>)
    4. interoperability (https://gitlab.com/holosphere4/holoBridges)
6. Distribute content to the following:
    1. white paper,
    2. [git] repository
    3. hyperlink guide to [hologuide index][hgi] in clickup docs https://app.clickup.com/2497563/v/dc/2c70v-596/2c70v-225 
    4. add as ML component on [MLP clickup doc][MLPC]
7. Model it ... 
   
[ha]: https://qwant.com/?q=holoAtom
[wf]: https://workflowy.com/#/7db22a57d98c
[hap]: https://mensual.framapad.org/p/holoAtom
[hl]: https://qwant.com/?q=holoLink
[git]: https://gitlab.com/dashboard/projects
[hgi]: https://app.clickup.com/2497563/v/dc/2c70v-596/2c70v-225
[MLPC]: https://app.clickup.com/2497563/v/dc/2c70v-630/2c70v-259


